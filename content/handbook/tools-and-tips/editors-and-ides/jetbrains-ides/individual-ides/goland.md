---
title: "GoLand"
---

## Overview

Website: https://www.jetbrains.com/go/

Best for: Applications written primarily in Go.

## Common Jetbrains Setup and Configuration

Jetbrains IDEs are standardized, so much of the setup and configuration information applies to all IDEs, and can be found under [Common Jetbrains Setup and Configuration]../../setup-and-config).

Specific config for this RubyMine can be found below at [GoLand-specific Setup and Configuration](#goland-specific-setup-and-config)

## GoLand-specific Setup and Configuration

Placeholder for future content...
