---
title: "RubyMine"
---

## Overview

Website: https://www.jetbrains.com/ruby/

Best for: editing Ruby or Rails applications, which can include Javascript/Typescript and most other
web technologies.

## Common Jetbrains Setup and Configuration

Jetbrains IDEs are standardized, so much of the setup and configuration information applies to all IDEs, and can be found under [Common Jetbrains Setup and Configuration](../../setup-and-config).

Specific config for this RubyMine can be found below at [RubyMine-specific Setup and Configuration](#rubymine-specific-setup-and-config)

## RubyMine-specific Setup and Configuration

Placeholder for future content...
