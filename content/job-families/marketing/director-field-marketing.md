---
title: "Director of Field Marketing"
summary: Support our Sales Teams, globally, build and lead a global team of field marketers aligned to our regional sales directors.
---

GitLab is looking for a highly motivated, sales-focused field marketing leader to support our Sales Teams, globally. The Director, Field Marketing position is responsible for building and leading a global team of field marketers aligned to our regional sales directors.

A qualified Director, Field Marketing has strong understanding of sales-focused marketing as well as our audiences of enterprise IT leaders, IT ops practitioners, and developers. They enjoy working with sales leaders to understand the needs of the sales teams, and meeting those needs through effective partnership, strategy, and execution.

## Job Grade

The Director, Field Marketing is a [grade 10](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities

- Hire, train, coach, and lead a global team of field marketing managers.
- Meet or exceed our annual plan to grow demand for each of our sales teams.
- Define what field marketing needs from corporate marketing, product marketing, online growth, marketing operations, and marketing program management to ensure regional sales support.
- Ensure field marketing adheres to global brand marketing standards, and is aligned to product marketing strategy.
- Ensure our global channel marketing strategy can be effectively deployed regionally by enabling our resellers with campaign components and guidelines.
- Help global marketing functions refine their strategy based on regional feedback.
- Partner with sales leadership on territory planning.
- Set the standard for effective account-based marketing practices, worldwide, and ensure it is documented and updated in our handbook.
- Set the standard to ensure effective collaboration across marketing and sales roles for regional campaign execution, and document those standards in our handbook.
- Improve field marketing campaign efficiency in terms of ROI and CAC:LTV.
- Ensure key regions have agency support for translating and regionalizing corporate marketing content.
- Manage field marketing budget and hiring plan.
- Administer variable compensation reporting for field marketers.
- Be an advocate for the sales department and help the rest of the marketing department understand their priorities.
- Be an advocate for the marketing department and help the sales department understand the marketing department's priorities.
- Ensure field marketing contributes productively to the quarterly business reviews of the sales team they support.

## Requirements

- Past experience delivering, accelerating, and expanding sales pipeline through regional marketing.
- 7+ years of marketing experience.
- People management experience.
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external stakeholders.
- Ability to empathize with the needs and experiences of IT leaders, IT ops practitioners, and developers.
- Extremely detail-oriented and organized, and able to meet deadlines.
- You share our [values](/handbook/values/), and work in accordance with those values.
- A passion and strong understanding of the developer tools, IT operations tools, and/or IT security markets.
- Strong understanding of marketing to financial services / fintech, federal agencies, healthcare, automotive, tech, and energy industry verticals.
- Experience with supporting both direct sales and channel sales teams.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)
- Ability to use GitLab
- Ability to travel if needed and comply with the company’s travel policy. If employed by GitLab Federal, LLC, team members need to also comply with the applicable vaccination policies.

## Career Ladder

The next step in the Director, Field Marketing job family is not yet defined at GitLab.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/).

- Qualified candidates will be invited to schedule a [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule an interview with the Senior Director of Revenue Marketing.
- Candidates will then be invited to schedule interviews with the Senior Director of Corporate Marketing and the Manager of Marketing Programs.
- Finally, our CMO may conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).
